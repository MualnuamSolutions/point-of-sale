<?php
namespace Mualnuam;
use Sales, Stocks, SalesItems, Products, Units, Distributions, OutletsStocks, OutletsStocksReturns, DB, Settings;

class Report
{
    public static function stock($input)
    {
        $t_sales = (new Sales)->getTable();
        $t_stocks = (new Stocks)->getTable();
        $t_sales_items = (new SalesItems)->getTable();
        $t_products = (new Products)->getTable();
        $t_units = (new Units)->getTable();
        $t_distributions = (new Distributions)->getTable();

        $sold_qty_query = "SELECT SUM(quantity) FROM {$t_sales_items} JOIN {$t_sales} ON {$t_sales}.id={$t_sales_items}.sales_id";
        $sold_sp_query = "SELECT sp FROM {$t_sales_items}";
        $sold_total_query = "SELECT SUM(total) FROM {$t_sales_items}";
        $stock_receive_qty_query = "SELECT SUM(quantity) FROM {$t_stocks} WHERE {$t_products}.id={$t_stocks}.product_id";
        $opening_stock_query = "SELECT SUM(quantity) FROM {$t_stocks} WHERE {$t_products}.id={$t_stocks}.product_id";
        $distribution_query = "SELECT SUM(quantity) FROM {$t_distributions} WHERE {$t_distributions}.product_id={$t_products}.id";
        
        $q0 = " WHERE {$t_sales_items}.product_id={$t_stocks}.product_id";
        $sold_qty_query .= $q0;
        $sold_sp_query .= $q0;
        $sold_total_query .= $q0;
        $opening_sold_query = $sold_qty_query;

        if (array_key_exists('from', $input) && strlen($input['from'])) {
            $q1 = " AND DATE({$t_sales_items}.created_at) >= '" . date('Y-m-d', strtotime($input['from'])) . "'";
            $sold_qty_query .= $q1;
            $sold_sp_query .= $q1;
            $sold_total_query .= $q1;

            // product received on or after 'from' date
            $stock_receive_qty_query .= " AND DATE({$t_stocks}.created_at) >= '" . date('Y-m-d', strtotime($input['from'])) . "'";

            // product stock before 'from' date
            $opening_stock_query .= " AND DATE({$t_stocks}.created_at) < '" . date('Y-m-d', strtotime($input['from'])) . "'";
            // product sold before 'from' date
            $opening_sold_query .= " AND DATE({$t_sales_items}.created_at) < '" . date('Y-m-d', strtotime($input['from'])) . "'";
        }

        if (array_key_exists('to', $input) && strlen($input['to'])) {
            $q2 = " AND DATE({$t_sales_items}.created_at) <= '" . date('Y-m-d', strtotime($input['to'])) . "'";
            $sold_qty_query .= $q2;
            $sold_sp_query .= $q2;
            $sold_total_query .= $q2;

            // product received on or before 'to' date
            $stock_receive_qty_query .= " AND DATE({$t_stocks}.created_at) <= '" . date('Y-m-d', strtotime($input['to'])) . "'";
        }

        if (array_key_exists('outlet_id', $input) && strlen($input['outlet_id'])) {
             $q3 = " AND {$t_sales}.outlet_id = {$input['outlet_id']}";
             $sold_qty_query .= $q3;
             $sold_sp_query .= $q3;
             $sold_total_query .= $q3;
        }

        $sold_sp_query .= " LIMIT 1";

        $products = Products::leftJoin($t_stocks, $t_stocks.'.product_id', '=', $t_products.'.id')
            ->leftJoin($t_units, $t_units.'.id', '=', $t_products.'.unit_id')
            ->where(function ($query) use ($input, $t_products) {
                if (array_key_exists('group', $input) && strlen($input['group']))
                    $query->where($t_products . '.type_id', '=', $input['group']);
            })
            ->select(
                "{$t_products}.name",
                "{$t_products}.product_code",
                "{$t_products}.cp",
                "{$t_units}.name as unit",

                DB::raw("({$opening_stock_query}) as opening_stock_qty"),
                DB::raw("({$opening_sold_query}) as opening_sold_qty"),

                DB::raw("({$stock_receive_qty_query}) as received_qty"),

                DB::raw("({$sold_qty_query}) as sold_qty"),
                DB::raw("($sold_sp_query) as sp"),
                
                DB::raw("($distribution_query) as distribution_qty"),

                DB::raw("($sold_total_query) as sold_total")
            )
            ->groupBy("{$t_products}.id")
            ->orderBy('name', 'asc')
            ->orderBy("{$t_products}.id", 'asc')
            ->get();

        return $products;
    }

    public static function outletStock($input)
    {
        $t_sales = (new Sales)->getTable();
        $t_stocks = (new Stocks)->getTable();
        $t_sales_items = (new SalesItems)->getTable();
        $t_products = (new Products)->getTable();
        $t_units = (new Units)->getTable();
        $t_distributions = (new Distributions)->getTable();
        $t_outlets_stocks = (new OutletsStocks)->getTable();
        $t_stocks_returns = (new OutletsStocksReturns)->getTable();

        $opening_received_stock_query = "SELECT SUM(quantity) FROM {$t_distributions} WHERE {$t_products}.id={$t_distributions}.product_id";
        $opening_returned_stock_query = "SELECT SUM(quantity) FROM {$t_stocks_returns} WHERE {$t_products}.id={$t_stocks_returns}.product_id AND status='Approved'";
        $opening_sold_qty_query = "SELECT SUM(quantity) FROM {$t_sales_items} JOIN {$t_sales} ON {$t_sales}.id={$t_sales_items}.sales_id";
        
        $received_stock_query = "SELECT SUM(quantity) FROM {$t_distributions} WHERE {$t_products}.id={$t_distributions}.product_id";
        $returned_stock_query = "SELECT SUM(quantity) FROM {$t_stocks_returns} WHERE {$t_products}.id={$t_stocks_returns}.product_id AND status='Approved'";
        $sold_qty_query = "SELECT SUM(quantity) FROM {$t_sales_items} JOIN {$t_sales} ON {$t_sales}.id={$t_sales_items}.sales_id";
        $sold_total_query = "SELECT SUM({$t_sales}.total) FROM {$t_sales_items} JOIN {$t_sales} ON {$t_sales}.id={$t_sales_items}.sales_id";

        $q0 = " WHERE {$t_sales_items}.product_id={$t_outlets_stocks}.product_id";
        $opening_sold_qty_query .= $q0;
        $sold_qty_query .= $q0;
        $sold_total_query .= $q0;

        if (array_key_exists('from', $input) && strlen($input['from'])) {
            $q1 = " AND DATE({$t_sales}.sales_date) >= '" . date('Y-m-d', strtotime($input['from'])) . "'";
            $sold_qty_query .= $q1;
            $sold_total_query .= $q1;

            // stock received before 'from' date
            $opening_received_stock_query .= " AND DATE({$t_distributions}.created_at) < '" . date('Y-m-d', strtotime($input['from'])) . "'";
            // stock returned before 'from' date
            $opening_returned_stock_query .= " AND DATE({$t_stocks_returns}.created_at) < '" . date('Y-m-d', strtotime($input['from'])) . "'";
            // product sold before 'from' date
            $opening_sold_qty_query .= " AND DATE({$t_sales}.sales_date) < '" . date('Y-m-d', strtotime($input['from'])) . "'";
            // stock received on or after 'from' date
            $received_stock_query .= " AND DATE({$t_distributions}.created_at) >= '" . date('Y-m-d', strtotime($input['from'])) . "'";
            // stock returned on or after 'from' date
            $returned_stock_query .= " AND DATE({$t_stocks_returns}.created_at) >= '" . date('Y-m-d', strtotime($input['from'])) . "'";
        }

        if (array_key_exists('to', $input) && strlen($input['to'])) {
            // stock received on or before 'to' date
            $received_stock_query .= " AND DATE({$t_distributions}.created_at) <= '" . date('Y-m-d', strtotime($input['to'])) . "'";
            // stock returned on or before 'to' date
            $returned_stock_query .= " AND DATE({$t_stocks_returns}.created_at) <= '" . date('Y-m-d', strtotime($input['to'])) . "'";
            // product sold on or before 'to' date
            $sold_qty_query .= " AND DATE({$t_sales}.sales_date) <= '" . date('Y-m-d', strtotime($input['to'])) . "'";
            $sold_total_query .= " AND DATE({$t_sales}.sales_date) <= '" . date('Y-m-d', strtotime($input['to'])) . "'";
        }

        if (array_key_exists('outlet', $input) && strlen($input['outlet'])) {
             $opening_received_stock_query .= " AND {$t_distributions}.outlet_id = {$input['outlet']}";
             $opening_returned_stock_query .= " AND {$t_stocks_returns}.outlet_id = {$input['outlet']}";
             $opening_sold_qty_query .= " AND {$t_sales}.outlet_id = {$input['outlet']}";
             
             $received_stock_query .= " AND {$t_distributions}.outlet_id = {$input['outlet']}";
             $returned_stock_query .= " AND {$t_stocks_returns}.outlet_id = {$input['outlet']}";
             $sold_qty_query .= " AND {$t_sales}.outlet_id = {$input['outlet']}";
             $sold_total_query .= " AND {$t_sales}.outlet_id = {$input['outlet']}";
        }

        $products = Products::join($t_outlets_stocks, $t_outlets_stocks.'.product_id', '=', $t_products.'.id')
            ->leftJoin($t_units, $t_units.'.id', '=', $t_products.'.unit_id')
            ->where(function ($query) use ($input, $t_products, $t_outlets_stocks) {
                if (array_key_exists('group', $input) && strlen($input['group']))
                    $query->where($t_products . '.type_id', '=', $input['group']);
                if (array_key_exists('outlet', $input) && strlen($input['outlet']))
                    $query->where($t_outlets_stocks . '.outlet_id', '=', $input['outlet']);
            })
            ->select(
                "{$t_products}.name",
                "{$t_products}.product_code",
                "{$t_products}.cp",
                "{$t_products}.sp",
                "{$t_units}.name as unit",

                DB::raw("({$opening_received_stock_query}) as opening_received_stock_qty"),
                DB::raw("({$opening_returned_stock_query}) as opening_returned_stock_qty"),
                DB::raw("({$opening_sold_qty_query}) as opening_sold_qty"),

                DB::raw("({$received_stock_query}) as received_stock_qty"),
                DB::raw("({$returned_stock_query}) as returned_stock_qty"),
                DB::raw("({$sold_qty_query}) as sold_qty"),
                DB::raw("($sold_total_query) as sold_total")
            )
            ->groupBy("{$t_products}.id")
            ->orderBy('name', 'asc')
            ->orderBy("{$t_products}.id", 'asc')
            ->get();
        
        return $products;
    }

    public static function fastMoving($outlet_id = null, $limit = 30)
    {
        $limit = Settings::getData('fast_moving', $limit);

        $t_sales = (new Sales)->getTable();
        $t_stocks = (new Stocks)->getTable();
        $t_sales_items = (new SalesItems)->getTable();
        $t_products = (new Products)->getTable();
        $t_units = (new Units)->getTable();
        $t_distributions = (new Distributions)->getTable();
        $t_outlets_stocks = (new OutletsStocks)->getTable();
        $t_stocks_returns = (new OutletsStocksReturns)->getTable();

        if($outlet_id) {
            return null; // do
        }
        else {
            $stock_upto_last_year_qty = "SELECT SUM(quantity) FROM {$t_stocks} WHERE {$t_products}.id={$t_stocks}.product_id AND YEAR(created_at) < YEAR(NOW())";
            $sold_upto_last_year_qty = "SELECT SUM(quantity) FROM {$t_sales_items} LEFT JOIN {$t_sales} ON {$t_sales}.id={$t_sales_items}.sales_id WHERE {$t_sales_items}.product_id={$t_stocks}.product_id AND YEAR({$t_sales}.sales_date) < YEAR(NOW())";
            $stock_query = "SELECT SUM(quantity) FROM {$t_stocks} WHERE {$t_products}.id={$t_stocks}.product_id AND YEAR(created_at)=YEAR(NOW())";
            $sold_query = "SELECT SUM(quantity) FROM {$t_sales_items} LEFT JOIN {$t_sales} ON {$t_sales}.id={$t_sales_items}.sales_id WHERE {$t_sales_items}.product_id={$t_stocks}.product_id AND YEAR({$t_sales}.sales_date)=YEAR(NOW())" ;

            $products = Products::join($t_stocks, $t_stocks.'.product_id', '=', $t_products.'.id')
                ->leftJoin($t_units, $t_units.'.id', '=', $t_products.'.unit_id')
                ->select(
                    "{$t_products}.id",
                    "{$t_products}.name",
                    "{$t_products}.product_code",
                    "{$t_units}.name as unit",

                    DB::raw("@stock_upto_last_year_qty := ({$stock_upto_last_year_qty}) as stock_upto_last_year_qty"),
                    DB::raw("@sold_upto_last_year_qty := ({$sold_upto_last_year_qty}) as sold_upto_last_year_qty"),
                    DB::raw("@stock_qty := ({$stock_query}) as stock_qty"),
                    DB::raw("@current_stock_qty :=  IFNULL(@stock_upto_last_year_qty,0)-IFNULL(@sold_upto_last_year_qty, 0) + @stock_qty as current_stock_qty"),
                    DB::raw("@sold_qty := ({$sold_query}) as sold_qty"),
                    DB::raw("round((@sold_qty/@current_stock_qty)*100,2) as percentage")
                )
                ->groupBy("{$t_products}.id")
                ->orderBy('percentage', 'desc')
                ->take($limit)
                ->get();
            
            return $products;
        }
    }

    /*
     * Type: current_month, last_30_days, last_six_months, current_year
     */
    public static function topSellingProducts($type = 'current_month', $limit = 10)
    {
        $t_sales = (new Sales)->getTable();
        $t_stocks = (new Stocks)->getTable();
        $t_sales_items = (new SalesItems)->getTable();
        $t_products = (new Products)->getTable();
        $t_units = (new Units)->getTable();
        $t_distributions = (new Distributions)->getTable();
        $t_outlets_stocks = (new OutletsStocks)->getTable();
        $t_stocks_returns = (new OutletsStocksReturns)->getTable();

        $sold_query = "SELECT SUM(quantity) FROM {$t_sales_items} LEFT JOIN {$t_sales} ON {$t_sales}.id={$t_sales_items}.sales_id WHERE {$t_sales_items}.product_id={$t_products}.id";
        
        if($type == 'current_month')
            $sold_query .= " AND YEAR({$t_sales}.sales_date)=YEAR(NOW()) AND MONTH({$t_sales}.sales_date)=MONTH(NOW())";
        if($type == 'last_30_days')
            $sold_query .= " AND DATE({$t_sales}.sales_date) BETWEEN DATE(DATE_SUB(NOW(), INTERVAL 30 DAY)) AND DATE(NOW())";
        if($type == 'last_six_months')
            $sold_query .= "  AND DATE({$t_sales}.sales_date) BETWEEN DATE(DATE_SUB(NOW(), INTERVAL 6 MONTH)) AND DATE(NOW())";
        if($type == 'current_year')
            $sold_query .= " AND YEAR({$t_sales}.sales_date)=YEAR(NOW())";

        $products = Products::join($t_sales_items, $t_sales_items.'.product_id', '=', $t_products.'.id')
            ->leftJoin($t_sales, $t_sales.'.id', '=', $t_sales_items.'.sales_id')
            ->leftJoin($t_units, $t_units.'.id', '=', $t_products.'.unit_id')
            ->select(
                "{$t_products}.id",
                "{$t_products}.name",
                "{$t_products}.product_code",
                "{$t_units}.name as unit",

                DB::raw("@sold_qty := ({$sold_query}) as sold_qty")
            )
            ->groupBy("{$t_products}.id")
            ->orderBy('sold_qty', 'desc')
            ->take($limit)
            ->get();
        
        return $products;
    }
}