<?php
use \Mualnuam\Permission;
use \Mualnuam\Report;

class HomeController extends BaseController
{
	public function index()
	{
        $fastMovingProducts = Report::fastMoving();
		return View::make('home', compact('fastMovingProducts'));
	}
}
