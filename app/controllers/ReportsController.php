<?php
use \Mualnuam\Report;

class ReportsController extends \BaseController
{

    public function __construct()
    {
        $this->beforeFilter('sentry');
        $this->user = Sentry::getUser();
    }

    public function stock()
    {
        // Products::cleanupStock(); 
        // exit;

        $input = Input::all();
        $outlets = SalesOutlets::dropdownList();
        $types = Types::dropdownList(false);
        $action = Input::get('action');

        $topSellingProducts = [];

        if ($action == 'view' || $action == 'export') {
            if( isset($input['outlet']) && $input['outlet'] == 0 )
                $products = Report::stock($input);
            else
                $products = Report::outletStock($input);

            $group = Types::find($input['group']);

            $excelTitle = 'STOCK REPORT ';
            if( $input['from'] == $input['to'])
                $excelTitle .= $input['from'];
            else
                $excelTitle .= 'BETWEEN '.date('d/m/Y', strtotime($input['from'])).' to '.date('d/m/Y', strtotime($input['to']));

            if( isset($input['outlet']) && $input['outlet'] == 0 ) {
                if ($action == 'export') {
                    // return View::make('reports.stock-export', compact('excelTitle', 'products', 'group'));

                    Excel::create('New file', function($excel) use($excelTitle, $products, $group) {                        
                        $excel->setTitle($excelTitle);
                        $excel->sheet('New sheet', function($sheet) use($excelTitle, $products, $group) {
                            $sheet->loadView('reports.stock-export', compact('excelTitle', 'products', 'group'));
                        });
                    })->export('xlsx');
                    exit('Exported');
                }
                else
                    return View::make('reports.stock-print', compact('input', 'products', 'group'));
            }
            else {
                $outlet = SalesOutlets::find($input['outlet']);
                if ($action == 'export') {
                    Excel::create('New file', function($excel) use($input, $products, $group) {
                        $excel->sheet('New sheet', function($sheet) use($input, $products, $group) {
                            $sheet->loadView('reports.stock-print-outlet', compact('input', 'products', 'group'));
                        });
                    });
                }
                else
                    return View::make('reports.stock-print-outlet', compact('input', 'products', 'group', 'outlet'));
            }
        }
        else {
            $topSellingProducts['current_month'] = Report::topSellingProducts('current_month');
            $topSellingProducts['last_30_days'] = Report::topSellingProducts('last_30_days');
            $topSellingProducts['last_six_months'] = Report::topSellingProducts('last_six_months');
            $topSellingProducts['current_year'] = Report::topSellingProducts('current_year');
        }

        return View::make('reports.stock', compact('input', 'outlets', 'types', 'topSellingProducts'));
    }

}
