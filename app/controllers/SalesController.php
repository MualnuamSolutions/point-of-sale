<?php

class SalesController extends \BaseController
{

    public function __construct()
    {
        $this->beforeFilter('sentry');
        $this->user = Sentry::getUser();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $input = Input::all();
        $outlets = SalesOutlets::dropdownList();
        $types = Types::dropdownList(false); // get dropdown array without select text

        $sales = Sales::with('items', 'customer')
            ->where(function ($query) use ($input) {

                if (array_key_exists('from', $input) && strlen($input['from']))
                    $query->where(DB::raw('DATE(created_at)'), '>=', date('Y-m-d', strtotime($input['from'])));

                if (array_key_exists('to', $input) && strlen($input['to']))
                    $query->where(DB::raw('DATE(created_at)'), '<=', date('Y-m-d', strtotime($input['to'])));

                if($this->user->outlet_id != 0)
                    $query->where('outlet_id', '=', $this->user->outlet_id);
                else {
                    if (array_key_exists('outlet', $input) && $input['outlet'] != '')
                        $query->where('outlet_id', '=', $input['outlet']);
                }

                if (array_key_exists('status', $input) && $input['status'] != '')
                    $query->where('status', '=', $input['status']);
            })
            ->orderBy('sales_date', 'desc')
            ->paginate(20);

        return View::make('sales.index', compact('sales', 'index', 'input', 'outlets', 'types'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::all();
        $products = Products::filter($input, 25);
        $outlets = SalesOutlets::dropdownList(true);
        $types = Types::dropdownList();

        return View::make('sales.create', compact('products', 'types', 'input', 'outlets'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validator = Validator::make(Input::all(), Sales::$rules);

        if ($validator->passes()) {

            // Create customer if required
            $customer = new Customers;
            if (Input::get('name')) {
                $customer->name = Input::get('name');
                $customer->address = Input::get('address');
                $customer->contact = Input::get('contact');
                $customer->save();
            }


            $sale = new Sales;
            $sale->reference_no = '';
            $sale->customer_id = $customer->id ? $customer->id : 0;
            $sale->outlet_id = Input::get('outlet_id', $this->user->outlet_id);
            $sale->discount = Input::get('discount');
            $sale->paid = Input::get('paid');
            $sale->total = Input::get('grandtotal');
            $sale->notes = Input::get('notes');
            $sale->sales_date = date('Y-m-d', strtotime( Input::get('sales_date') ));
            $sale->status = Input::get('paid') == Input::get('grandtotal') ? 'completed' : 'credit';

            if ($sale->save()) {
                $sale->reference_no = 'SALE-' . date('Ymd') . '-' . str_pad($sale->id, 3, 0, STR_PAD_LEFT);
                $sale->save();

                foreach (Input::get('cart') as $productId => $item) {

                    $itemDiscount = 0;
                    if ($item['discount_type'] == 'fixed')
                        $itemDiscount = $item['discount_amount'] * $item['quantity'];
                    elseif ($item['discount_type'] == 'percentage')
                        $itemDiscount = (($item['discount_amount'] / 100) * $item['sp']) * $item['quantity'];


                    $salesItem = new SalesItems;
                    $salesItem->sales_id = $sale->id;
                    $salesItem->product_id = $productId;
                    $salesItem->cp = $item['cp'];
                    $salesItem->sp = $item['sp'];
                    $salesItem->quantity = $item['quantity'];
                    $salesItem->total = ($item['quantity'] * $item['sp']) - $itemDiscount;
                    $salesItem->discount_type = $item['discount_type'];
                    $salesItem->discount_amount = $item['discount_amount'];
                    $salesItem->discount_total = $itemDiscount;
                    $salesItem->save();

                    if($this->user->outlet_id != 0) {
                        $outletstock = OutletsStocks::where('product_id','=',$productId)
                            ->where('outlet_id','=',$this->user->outlet_id)
                            ->first();
                        $outletstock->quantity = $outletstock->quantity - $salesItem->quantity;
                        $outletstock->save();
                    }
                    else {
                        Products::updateStock($productId);
                    }
                }
            }

            return Redirect::route('sales.edit', $sale->id)
                ->with('success', 'New sale added successfully');
        } else {
            return Redirect::route('sales.create')
                ->with('error', 'Please add some items first')
                ->withErrors($validator)
                ->withInput(Input::all());
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $sale = Sales::with('customer', 'items')->find($id);

        return View::make('sales.show', compact('sale'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $sale = Sales::with('customer', 'items')->find($id);

        return View::make('sales.edit', compact('sale'));
    }

    public function returnitem($id)
    {
        $sales_items = SalesItems::where('sales_id', '=', $id)->select('product_id')->get();
        SalesItems::where('sales_id', '=', $id)->delete();
        Sales::destroy($id);
        
        if($this->user->outlet_id != 0) {
            
        }

        else {
            foreach($sales_items as $item)
                Products::updateStock($item->product_id);
        }



        $sales = SalesItems::where('sales_id','=',$id)->get();
        if($sales)
        {    
            foreach ($sales as $sale) 
            {
                
                if($this->user->outlet_id != 0)
                {
                    
                    $outletstock = OutletsStocks::where('product_id','=',$sale->product_id)
                        ->where('outlet_id','=',$this->user->outlet_id)
                        ->first();
                    if($outletstock)
                    {
                        $outletstock->quantity = $outletstock->quantity + $sale->quantity;
                        $outletstock->save();
                        SalesItems::destroy($sale->id);
                    }
                }
                else
                {
                    $product = Products::find($sale->product_id);
                    if($product)
                    {
                       // var_dump($product);exit();
                        $product->quantity = $product->quantity + $sale->quantity;
                        $product->save();
                        SalesItems::destroy($sale->id);
                    }
                }
                
            }
        
        Sales::destroy($id);

        return Redirect::route('sales.index')
                ->with('success', 'Item returned successfully.');
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $validator = Validator::make(Input::all(), Sales::$rules);

        if ($validator->passes()) {
            $customerId = Input::get('customer_id', null);
            // Create customer if required
            $customer = new Customers;
            
            if ($customerId)
            {
                $customer = Customers::find($customerId);
            }
            if (Input::get('name')) {
                $customer->name = Input::get('name');
                $customer->address = Input::get('address');
                $customer->contact = Input::get('contact');
                $customer->save();
            }


            $sale = Sales::find($id);
            $sale->customer_id = $customer->id ? $customer->id : 0;
            $sale->outlet_id = $this->user->outlet_id;
            $sale->paid = Input::get('paid');
            $sale->notes = Input::get('notes');
            $sale->sales_date = date('Y-m-d', strtotime( Input::get('sales_date') ));
            $sale->status = Input::get('paid') == Input::get('grandtotal') ? 'completed' : 'credit';
            $sale->save();

            return Redirect::route('sales.edit', $id)
                ->with('success', 'Sale updated successfully');
        } else {
            return Redirect::route('sales.edit', $id)
                ->withErrors($validator)
                ->withInput(Input::all());
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!$id)
            return Redirect::route('sales.index')
                ->with('error', 'Please provide sales record id');

        $sale = Sales::find($id);

        if (empty($sale))
            return Redirect::route('sales.index')
                ->with('error', 'Sale record not found');
        $sales_items = SalesItems::where('sales_id', '=', $id)->select('product_id')->get();
        SalesItems::where('sales_id', '=', $id)->delete();
        Sales::destroy($id);
        
        foreach($sales_items as $item)
            Products::updateStock($item->product_id);


        return Redirect::route('sales.index')
            ->with('success', 'Sales record deleted successfully');
    }


}
