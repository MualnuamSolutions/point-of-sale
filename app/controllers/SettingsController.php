<?php

class SettingsController extends \BaseController
{
    public function __construct()
    {
        $this->beforeFilter('sentry');
    }
   
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    
    public function index()
    {
        return View::make('settings.index');
    }


    public function store()
    {
        if(Input::has('store_name')) {
            $setting = Settings::whereOptionKey('store_name')->first();
            $setting->option_data = Input::get('store_name');
            $setting->save();
        }

        if(Input::has('low_stock_warning_qty')) {
            $setting = Settings::whereOptionKey('low_stock_warning_qty')->first();
            if($setting) {
                $setting->option_data = Input::get('low_stock_warning_qty', 0);
            }
            else {
                $setting = new Settings;
                $setting->option_key = 'low_stock_warning_qty';
                $setting->option_title = 'Low Stock Warning Quantity';
                $setting->option_data = Input::get('low_stock_warning_qty', 0);
            }
            $setting->save();
        }

        if(Input::has('fast_moving')) {
            $setting = Settings::whereOptionKey('fast_moving')->first();
            if($setting) {
                $setting->option_data = Input::get('fast_moving', 10);
            }
            else {
                $setting = new Settings;
                $setting->option_key = 'fast_moving';
                $setting->option_title = 'Display Limit For Fast Moving Products';
                $setting->option_data = Input::get('fast_moving', 10);
            }
            $setting->save();
        }

        return Redirect::route('settings.index')
            ->with('success', 'Settings updated successfully');
    }

}
