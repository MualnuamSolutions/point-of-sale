<?php

class StockReturnsController extends \BaseController
{
    public function __construct()
    {
        $this->beforeFilter('sentry');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $outlets = SalesOutlets::dropdownList();
        $status = Input::get('status', null);
        $outletId = Input::get('outlet', $this->loggedUser()->outlet_id);

        $returnsStocks = OutletsStocksReturns::where(function($q) use($status, $outletId) {
                if($outletId != 'all' && $outletId != 0)
                    $q->whereOutletId($outletId);
                if($status != '')
                    $q->whereStatus($status);
            })
            ->orderBy('created_at','desc')
            ->paginate(20);
        //var_dump($returnsStocks);exit();
        return View::make('stockreturns.index', compact('returnsStocks', 'outlets'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        
    }

    //Stock Return 
    public function returnStock($id)
    {
        $outletsstocks = OutletsStocks::find($id);
        return View::make('stocks.createreturn', compact('outletsstocks', 'id'));
    }

    public function approve($id)
    {
        $outletsStocksReturns = OutletsStocksReturns::find($id);
        
        if($outletsStocksReturns) {

            $outletsStocksReturns->status = "Approved";
            $outletsStocksReturns->save();

            // First update outlet stocks
            OutletsStocks::updateStock($outletsStocksReturns->product_id, $outletsStocksReturns->outlet_id);

            // Now reflect the updated stock on products stock
            Products::updateStock($outletsStocksReturns->product_id);

            return Redirect::route('stockreturns.index')
                ->with('success', 'Stock return completed');
        } else {
            return Redirect::route('stockreturns.index')
            ->with('error', 'No pending stock return for this product.');
        }
    }

    public function reject($id)
    {
        $outletsStocksReturns = OutletsStocksReturns::find($id);
         if(!$outletsStocksReturns) {
            return Redirect::route('stockreturns.index')
            ->with('error', 'No record found');
         }
         else if($outletsStocksReturns->status == "Pending...") {
            $outletsStocksReturns->status = "Rejected";
            $outletsStocksReturns->save();

            return Redirect::route('stockreturns.index')
                ->with('success', 'Stock return rejected');
         }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validator = Validator::make(Input::all(), OutletsStocksReturns::$returnrules);
        $outletsStocks = OutletsStocks::where('product_id','=',Input::get('product_id'))->where('outlet_id','=',Input::get('outlet_id'))->first();
        $stockId = Input::get('stock_id');
        
        if(Input::get('quantity') <= $outletsStocks->quantity) { 
            if($validator->passes()) {
                $stockreturn = new OutletsStocksReturns;
                $stockreturn->product_id = Input::get('product_id');
                $stockreturn->outlet_id = Input::get('outlet_id');
                $stockreturn->quantity = Input::get('quantity');
                $stockreturn->status = "Pending...";
                $stockreturn->comment = Input::get('comments');
                $stockreturn->save();
             
                return Redirect::route('products.index')
                    ->with('success', 'Stock return initiated successfully');
            }
            else {
             return Redirect::route('stockreturns.return', $stockId)
                ->withErrors($validator)
                ->withInput(Input::all());
            }
        }
        else
        {
            return Redirect::route('stockreturns.return', $stockId)
                ->with('error', 'Returned quantity should not be more than stock quantity');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


}
