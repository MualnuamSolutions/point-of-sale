<?php

class Products extends Eloquent
{
    protected $table = "products";
    public static $rules = [
        'name' => 'required',
        'type_id' => 'required',
        'cp' => 'required',
        'sp' => 'required',
        'quantity' => 'required',
        'unit_id' => 'required'
    ];
    public static $rules2 = [
        'name' => 'required',
        'type_id' => 'required',
        'unit_id' => 'required'
    ];

    public static $rules3 = [
        'discount' => 'required'
    ];

    public function setProductCode($product)
    {
        $product->product_code = "ZHC" . str_pad($product->type_id, 3, 0, STR_PAD_LEFT) . str_pad($product->id, 6, 0, STR_PAD_LEFT);
        $product->save();
    }

    public static function updateStock($productId)
    {
        $stockQuantity = Stocks::whereProductId($productId)->sum('quantity');
        $soldQuantity = SalesItems::where('product_id', '=', $productId)->sum('quantity');
        $outletStocksQuantity = OutletsStocks::where('product_id', '=', $productId)->sum('quantity');

        $product = Products::find($productId);
        // Note: We don't need to store total stock amount as we can derived it from stock table
        // $product->quantity = $stockQuantity - $outletStocksQuantity;
        $product->in_stock = $stockQuantity - $soldQuantity - $outletStocksQuantity;
        $product->save();

        // echo $stockQuantity.' - '.$soldQuantity.' - '.$outletStocksQuantity.' => '.$product->in_stock.'<br>';
    }

    public function type()
    {
        return $this->hasOne('Types', 'id', 'type_id');
    }

    public function unit()
    {
        return $this->hasOne('Units', 'id', 'unit_id');
    }

    public function discount()
    {
        return $this->belongsTo('Discounts', 'product_id');
    }

    public function stocks()
    {
        return $this->hasMany('Stocks', 'product_id');
    }

    public function discounts()
    {
        return $this->hasOne('Discounts','id','product_id');
    }

    public function color()
    {
        return $this->hasOne('Colors', 'id', 'color_id');
    }

    public static function dropdownList($exludeDiscounted = false)
    {
        $discountedProducts = Discounts::select('product_id')->get()->lists('product_id');
        if($exludeDiscounted && $discountedProducts)
            $products = Products::whereNotIn('id', Discounts::select('product_id')->get()->lists('product_id'))
                ->orderBy('name', 'asc')->select('id', DB::raw('CONCAT(name, " - Rs ",  sp) as name'))->get();
        else
            $products = Products::orderBy('name', 'asc')->select('id', DB::raw('CONCAT(name, " - Rs ",  sp) as name'))->get();

        return array('' => 'Select Product') + $products->lists('name', 'id');
    }

    public static function lowStock($outletId = 0, $product = 0)
    {
        $products = null;
        $outletStockTable = (new OutletsStocks)->getTable();
        $productTable = (new Products)->getTable();

        if($outletId != 0) {
            $products = Products::join($outletStockTable, $outletStockTable.'.product_id', '=', $productTable.'.id')
                ->where(function ($query) use ($outletId, $outletStockTable) {
                    $query->where('outlet_id', '=', $outletId);
                    $low_stock_warning_qty = Settings::getData('low_stock_warning_qty');
                    $query->where($outletStockTable.'.quantity', '<=', DB::raw('IF(`low_stock_warning_qty` = 0.00, '.$low_stock_warning_qty.',`low_stock_warning_qty`)'));
                })
                ->get();
        }
        else {
            $products = Products::where(function ($query) use ($outletId) {
                    $low_stock_warning_qty = Settings::getData('low_stock_warning_qty');
                    $query->where('in_stock', '<=', DB::raw('IF(`low_stock_warning_qty` = 0.00, '.$low_stock_warning_qty.',`low_stock_warning_qty`)'));
                })
                ->get();
        }

        return $products;
    }

    public static function filter($input, $limit = 24, $outletId = 0)
    {
        $products = null;
        $outletStockTable = (new OutletsStocks)->getTable();
        $productTable = (new Products)->getTable();

        if($outletId != 0) {
            // $query->whereIn('id', OutletsStocks::whereOutletId($outletId)->get()->lists('product_id'));
            $products = Products::join($outletStockTable, $outletStockTable.'.product_id', '=', $productTable.'.id')
                ->where(function ($query) use ($input, $outletId, $outletStockTable) {

                    $query->where('outlet_id', '=', $outletId);

                    if (array_key_exists('name', $input) && strlen($input['name']))
                        $query->where('name', 'LIKE', "%" . $input['name'] . "%");

                    if (array_key_exists('stock', $input) && strlen($input['stock'])) {
                        $low_stock_warning_qty = Settings::getData('low_stock_warning_qty');
                        if($input['stock'] == 'low_stock') {
                            $query->where($outletStockTable.'.quantity', '<=', DB::raw('IF(`low_stock_warning_qty` = 0.00, '.$low_stock_warning_qty.',`low_stock_warning_qty`)'));
                        }

                        elseif($input['stock'] == 'out_of_stock')
                            $query->where($outletStockTable.'.quantity', '<=', 0);
                    }

                    if (array_key_exists('name_code', $input) && strlen($input['name_code'])) {
                        $query->where(function ($query) use ($input) {
                            $query->where('name', 'LIKE', "%" . $input['name_code'] . "%");
                            $query->orWhere('product_code', 'LIKE', "%" . trim($input['name_code']) . "%");
                        });
                    }

                    if (array_key_exists('type', $input) && strlen($input['type']))
                        $query->whereTypeId($input['type']);

                    if (array_key_exists('entry_from', $input) && strlen($input['entry_from']))
                        $query->where(DB::raw('DATE(created_at)'), '>=', date('Y-m-d', strtotime($input['entry_from'])));

                    if (array_key_exists('entry_to', $input) && strlen($input['entry_to']))
                        $query->where(DB::raw('DATE(created_at)'), '<=', date('Y-m-d', strtotime($input['entry_to'])));

                    if (array_key_exists('unit', $input) && strlen($input['unit']))
                        $query->whereUnitId($input['unit']);

                    if (array_key_exists('barcode', $input) && strlen($input['barcode']))
                        $query->where('product_code', 'LIKE', "%" . trim($input['barcode']) . "%");

                })
                ->select('products.id', 'name', 'product_code',
                    'cp', 'sp', 'outlets_stocks.quantity as in_stock','unit_id','type_id',
                    'low_stock_warning_qty', 'outlets_stocks.id as stock_id',
                    DB::raw('CONCAT(name, " - Rs. ", cp , " / Rs. " , sp, ":", products.id) as nameprice'))
                ->orderBy('name', 'asc');
        }
        else {
            $products = Products::where(function ($query) use ($input, $outletId) {

                    if (array_key_exists('name', $input) && strlen($input['name']))
                        $query->where('name', 'LIKE', "%" . $input['name'] . "%");

                    if (array_key_exists('stock', $input) && strlen($input['stock'])) {
                        $low_stock_warning_qty = Settings::getData('low_stock_warning_qty');
                        if($input['stock'] == 'low_stock') {
                            $query->where('in_stock', '<=', DB::raw('IF(`low_stock_warning_qty` = 0.00, '.$low_stock_warning_qty.',`low_stock_warning_qty`)'));
                        }

                        elseif($input['stock'] == 'out_of_stock')
                            $query->where('in_stock', '<=', 0);
                    }

                    if (array_key_exists('name_code', $input) && strlen($input['name_code'])) {
                        $query->where(function ($query) use ($input) {
                            $query->where('name', 'LIKE', "%" . $input['name_code'] . "%");
                            $query->orWhere('product_code', 'LIKE', "%" . trim($input['name_code']) . "%");
                        });
                    }

                    if (array_key_exists('type', $input) && strlen($input['type']))
                        $query->whereTypeId($input['type']);

                    if (array_key_exists('entry_from', $input) && strlen($input['entry_from']))
                        $query->where(DB::raw('DATE(created_at)'), '>=', date('Y-m-d', strtotime($input['entry_from'])));

                    if (array_key_exists('entry_to', $input) && strlen($input['entry_to']))
                        $query->where(DB::raw('DATE(created_at)'), '<=', date('Y-m-d', strtotime($input['entry_to'])));

                    if (array_key_exists('unit', $input) && strlen($input['unit']))
                        $query->whereUnitId($input['unit']);

                    if (array_key_exists('barcode', $input) && strlen($input['barcode']))
                        $query->where('product_code', 'LIKE', "%" . trim($input['barcode']) . "%");

                })
                ->select('products.*', DB::raw('CONCAT(name, " - Rs. ", cp , " / Rs. " , sp, ":",id) as nameprice'))
                ->orderBy('name', 'asc');
        }

        if($limit == 0)
            return $products->get();
        else
            return $products->paginate($limit);
    }

    public static function autocompleteSearch($query = null)
    {
        $productTable = (new Products)->getTable();
        $discountTable = (new Discounts)->getTable();
        $outletStockTable = (new OutletsStocks)->getTable();
        $outletId = Sentry::getUser()->outlet_id;

        if($outletId != 0) {
            $products = Products::leftJoin($discountTable, $discountTable . '.product_id', '=', $productTable . '.id')
                ->leftJoin($outletStockTable, $outletStockTable . '.product_id', '=', $productTable . '.id')
                ->where(function($select) use ($query) {
                    $select->where('name', 'LIKE', '%' . $query . '%');
                    $select->orWhere('product_code', 'LIKE', '%' . $query . '%');
                })
                ->where($outletStockTable . '.in_stock', '>', 0)
                ->where($outletStockTable . '.outlet_id', '=', $outletId)
                ->select(
                    DB::raw("CONCAT(name, ' Rs ', sp, ' - In stock (', {$outletStockTable}.quantity, ')') as value"),
                    DB::raw("CONCAT(
                            '{\"id\":\"', {$productTable}.id, '\"',
                            ',\"product_code\":\"', product_code, '\"',
                            ',\"name\":\"', name, '\"',
                            ',\"sp\":\"', sp, '\"',
                            ',\"cp\":\"', cp, '\"',
                            ',\"in_stock\":\"', {$outletStockTable}.in_stock, '\"',
                            ',\"discount\":\"', amount, '\"',
                            ',\"discount_type\":\"', discount_type, '\"}'
                         ) as data"),
                    DB::raw("CONCAT(
                            '{\"id\":\"', {$productTable}.id, '\"',
                            ',\"product_code\":\"', product_code, '\"',
                            ',\"name\":\"', name, '\"',
                            ',\"sp\":\"', sp, '\"',
                            ',\"cp\":\"', cp, '\"',
                            ',\"in_stock\":\"', {$outletStockTable}.in_stock, '\"',
                            ',\"discount\":0',
                            ',\"discount_type\":\"fixed\"}'
                         ) as nodiscount")
                )
                ->orderBy('name', 'asc')
            ->get();
        }
        else {
            $products = Products::leftJoin($discountTable, $discountTable . '.product_id', '=', $productTable . '.id')
                ->where(function($select) use ($query) {
                    $select->where('name', 'LIKE', '%' . $query . '%');
                    $select->orWhere('product_code', 'LIKE', '%' . $query . '%');
                })
                ->where('in_stock', '>', 0)
                ->select(
                    DB::raw("CONCAT(name, ' Rs ', cp, ' / Rs ', sp, ' - In stock (', in_stock, ')') as value"),
                    DB::raw("CONCAT(
                            '{\"id\":\"', {$productTable}.id, '\"',
                            ',\"product_code\":\"', product_code, '\"',
                            ',\"name\":\"', name, '\"',
                            ',\"sp\":\"', sp, '\"',
                            ',\"cp\":\"', cp, '\"',
                            ',\"in_stock\":\"', in_stock, '\"',
                            ',\"discount\":\"', amount, '\"',
                            ',\"discount_type\":\"', discount_type, '\"}'
                         ) as data"),
                    DB::raw("CONCAT(
                            '{\"id\":\"', {$productTable}.id, '\"',
                            ',\"product_code\":\"', product_code, '\"',
                            ',\"name\":\"', name, '\"',
                            ',\"sp\":\"', sp, '\"',
                            ',\"cp\":\"', cp, '\"',
                            ',\"in_stock\":\"', in_stock, '\"',
                            ',\"discount\":0',
                            ',\"discount_type\":\"fixed\"}'
                         ) as nodiscount")
                )
                ->orderBy('name', 'asc')
                ->get();
        }

        $result = [
            'query' => $query,
            'suggestions' => $products->toArray()
        ];

        return $result;
    }


    // This method may be used to reset product stock in case it messed up.
    public static function cleanupStock()
    {
        $products = self::get();
        foreach($products as $p) {
            self::updateStock($p->id);
        }
    }

}
