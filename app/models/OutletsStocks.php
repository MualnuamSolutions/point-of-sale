<?php
class OutletsStocks extends Eloquent
{
	protected $table = "outlets_stocks";
	public $timestamps = false;
   /*public static $rules = [
         'name' => 'required',
         'address' => 'required'
   ];

   public static function dropdownList()
   {
      return array('' => 'Select Outlet') + self::orderBy('name', 'asc')->get()->lists('name', 'id');
   }*/
   
    public function product()
   {
      return $this->hasOne('Products', 'id','product_id');
   }

   public static function updateStock($productId, $outletId)
   {
      $t_sales = (new Sales)->getTable();
      $t_sales_items = (new SalesItems)->getTable();

      $receivedQuantity = Distributions::whereOutletId($outletId)
         ->whereProductId($productId)->sum('quantity');
      
      $soldQuantity = SalesItems::leftJoin($t_sales, $t_sales.'.id', '=', $t_sales_items.'.sales_id')
         ->whereOutletId($outletId)
         ->whereProductId($productId)
         ->sum('quantity');
      
      $returnedQuantity = OutletsStocksReturns::whereOutletId($outletId)
         ->whereProductId($productId)
         ->whereStatus('Approved')
         ->sum('quantity');

      $product = OutletsStocks::whereProductId($productId)->first();
      $product->quantity = $receivedQuantity - $soldQuantity - $returnedQuantity;
      $product->save();
   }

   
}
