<?php
class Settings extends Eloquent
{
   protected $table = "options";
   public $timestamps = false;

   public static $rules = [
         'option_key' => 'required',
         'option_data' => 'required',
   ];

   public static function getData($key='', $default = '')
   {
   		$setting = Settings::whereOptionKey($key)->pluck('option_data');
   		if(!$setting)
   			return $default;
   		else
   			return $setting;
   }
}
