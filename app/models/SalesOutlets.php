<?php
class SalesOutlets extends Eloquent
{
	protected $table = "sales_outlets";
   public static $rules = [
         'name' => 'required',
         'address' => 'required'
   ];

   public static function dropdownList($select = false)
   {
   		if($select)
      		return array('0' => 'Select Outlet') + self::orderBy('name', 'asc')->get()->lists('name', 'id');
      	else
      		return array('all' => 'All Outlets') + self::orderBy('name', 'asc')->get()->lists('name', 'id');
   }
}
