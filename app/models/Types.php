<?php
class Types extends Eloquent
{
	protected $table = "types";

   public static $rules = [
         'name' => 'required'
   ];

   public static function dropdownList( $select = true)
   {
   		if($select)
      		return array('' => 'Select Type') + Types::orderBy('name', 'asc')->get()->lists('name', 'id');
      	else
      		return Types::orderBy('name', 'asc')->get()->lists('name', 'id');
   }

}
