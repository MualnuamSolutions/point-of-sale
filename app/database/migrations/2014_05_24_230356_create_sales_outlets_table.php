<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesOutletsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sales_outlets',function($table)
      {
         $table->increments('id');
         $table->string('name');
         $table->string('address');
         $table->string('contact');
         $table->decimal('stock_cp', 10, 2)->default(0.00);
         $table->decimal('stock_sp', 10, 2)->default(0.00);
         $table->decimal('sales', 10, 2)->default(0.00);
         $table->decimal('deposit', 10, 2)->default(0.00);
         $table->timestamps();
      });
   }


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sales_outlets');
	}

}
