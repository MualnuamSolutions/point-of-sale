<?php

class OptionTableSeeder extends Seeder {

   /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run()
   {
      DB::table('options')->truncate();


      DB::table('options')->insert(array(
         array('option_key'=>'store_name', 'option_title'=>'Store Name', 'option_data'=>'ZOHANDCO Point of Sale'),
         ));

   }

}
