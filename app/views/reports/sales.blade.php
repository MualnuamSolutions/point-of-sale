@extends('layout')

@section('content')
   <div class="col-md-8 col-md-offset-2">
      <div class="row">
         <div class="panel panel-default">
            <div class="panel-heading">
               <h3 class="panel-title"><i class="fi-graph-pie"></i> Sales Report </h3>
            </div>
            <div class="panel-body sales-list">
               {{ Form::open(['url' => route('reports.sales'), 'method' => 'get', 'class' => 'sales-report form form-vertical', 'autocomplete' => 'off']) }}                     
                  <div class="row">
                     <div class="col-md-8">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="">Outlet</label>
                              {{ Form::select('outlet', $outlets, Input::get('outlet', null), ['class' => 'form-control input-sm']) }}
                           </div>                        
                        </div>

                        <div class="col-md-6">
                           <label for="">Group</label>
                           <div class="form-group">
                              {{ Form::select('group', ['' => 'All Groups'] + $types, Input::get('group', null), ['class' => 'form-control input-sm']) }}
                           </div>
                        </div>

                        <div class="col-md-6">
                           <label for="">Date From</label>
                           <div class="form-group">
                              {{ Form::text('from', Input::get('from', null), array('class' => 'datepicker-from form-control','placeholder' => 'Select Date From')) }}
                           </div>
                        </div>
                        
                        <div class="col-md-6">
                           <label for="">Date To</label>
                           <div class="form-group">
                              {{ Form::text('to', Input::get('to', null), array('class' => 'datepicker-to form-control','placeholder' => 'Select Date To')) }}
                           </div>
                        </div>

                        <div class="col-md-6">
                           <label for="">Status</label>
                           <div class="form-group">
                              {{ Form::select('status', ['' => 'All', 'completed' => 'Completed', 'credit' => 'Credit'], Input::get('status', null), ['class' => 'form-control input-sm']) }}
                           </div>
                        </div>
                        
                     </div>
                     <div class="col-md-4">
                        {{ Form::button('View / Print', ['class' => 'btn btn-md btn-primary', 'type' => 'button', 'onclick' => 'return viewPrint();']) }}
                        {{ Form::button('Export CSV', ['class' => 'btn btn-md btn-primary', 'type' => 'button', 'onclick' => 'return exportCsv()']) }}
                     </div>
                  </div>
               {{ Form::close() }}

            </div>
         </div>
      </div>
   </div>
@stop

@section('script')
<script type="text/javascript">
jQuery(function(){

});

function viewPrint()
{
   var formData = $('form.sales-report').serialize();
   
   var url = '/reports/sales?' + formData;
        url += '&action=view';

   window.open(url);
}

function exportCsv()
{
   var formData = $('form.sales-report').serialize();
   
   var url = '/reports/sales?' + formData;
        url += '&action=export';

   window.open(url);
}
</script>
@stop
