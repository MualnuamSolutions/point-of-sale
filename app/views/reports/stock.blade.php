@extends('layout')

@section('content')
   <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
         <div class="panel-heading">
            <h3 class="panel-title"><i class="fi-graph-pie"></i> Stock Report </h3>
         </div>
         <div class="panel-body sales-list">
            {{ Form::open(['url' => route('reports.stock'), 'method' => 'get', 'class' => 'stock-report form form-vertical', 'autocomplete' => 'off']) }}                     
               <div class="row">
                  <div class="col-md-8">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label for="">Outlet</label>
                           {{ Form::select('outlet', $outlets, Input::get('outlet', null), ['class' => 'form-control input-sm']) }}
                        </div>                        
                     </div>

                     <div class="col-md-6">
                        <label for="">Group</label>
                        <div class="form-group">
                           {{ Form::select('group', ['' => 'All Groups'] + $types, Input::get('group', null), ['class' => 'form-control input-sm']) }}
                        </div>
                     </div>
                     
                     <div class="col-md-6">
                        <label for="">Date From</label>
                        <div class="form-group">
                           {{ Form::text('from', Input::get('from', null), array('class' => 'datepicker-from form-control','placeholder' => 'Select Date From')) }}
                        </div>
                     </div>
                     
                     <div class="col-md-6">
                        <label for="">Date To</label>
                        <div class="form-group">
                           {{ Form::text('to', Input::get('to', null), array('class' => 'datepicker-to form-control','placeholder' => 'Select Date To')) }}
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     {{ Form::button('View / Print', ['class' => 'btn btn-md btn-primary', 'type' => 'button', 'onclick' => 'return viewPrint();']) }}
                     {{ Form::button('Export Excel', ['class' => 'btn btn-md btn-primary', 'type' => 'button', 'onclick' => 'return exportCsv()']) }}
                  </div>
               </div>
            {{ Form::close() }}

         </div>
      </div>
   </div>

   <div class="col-md-10 col-md-offset-1">
      <div class="row">
         <div class="col-md-6">
            <div class="panel panel-danger">
               <div class="panel-heading">
                  <h3 class="panel-title"><i class="fi-graph-pie"></i> Top 10 Selling Products of the Month </h3>
               </div>
               <div class="panel-body">
                  <table class="table table-condensed">
                     <thead>
                        <tr>
                           <th>#</th>
                           <th>Product</th>
                           <th>Sold</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach($topSellingProducts['current_month'] as $key => $product)
                        @if($product->sold_qty)
                        <tr>
                           <td>{{ $key+1 }}</td>
                           <td><a href="{{ url('products') }}?name_code={{ $product->product_code }}" target="_blank">{{ $product->name }}</a></td>
                           <td>{{ $product->sold_qty }} {{ $product->unit }}</td>
                        </tr>
                        @endif
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
         <div class="col-md-6">
            <div class="panel panel-danger">
               <div class="panel-heading">
                  <h3 class="panel-title"><i class="fi-graph-pie"></i> Top 10 Selling Products of Last 30 Days</h3>
               </div>
               <div class="panel-body">
                  <table class="table table-condensed">
                     <thead>
                        <tr>
                           <th>#</th>
                           <th>Product</th>
                           <th>Sold</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach($topSellingProducts['last_30_days'] as $key => $product)
                        @if($product->sold_qty)
                        <tr>
                           <td>{{ $key+1 }}</td>
                           <td><a href="{{ url('products') }}?name_code={{ $product->product_code }}" target="_blank">{{ $product->name }}</a></td>
                           <td>{{ $product->sold_qty }} {{ $product->unit }}</td>
                        </tr>
                        @endif
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>

      <div class="row">
         <div class="col-md-6">
            <div class="panel panel-success">
               <div class="panel-heading">
                  <h3 class="panel-title"><i class="fi-graph-pie"></i> Top 10 Selling Products of Last 6 Months </h3>
               </div>
               <div class="panel-body">
                  <table class="table table-condensed">
                     <thead>
                        <tr>
                           <th>#</th>
                           <th>Product</th>
                           <th>Sold</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach($topSellingProducts['last_six_months'] as $key => $product)
                        @if($product->sold_qty)
                        <tr>
                           <td>{{ $key+1 }}</td>
                           <td><a href="{{ url('products') }}?name_code={{ $product->product_code }}" target="_blank">{{ $product->name }}</a></td>
                           <td>{{ $product->sold_qty }} {{ $product->unit }}</td>
                        </tr>
                        @endif
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
         <div class="col-md-6">
            <div class="panel panel-success">
               <div class="panel-heading">
                  <h3 class="panel-title"><i class="fi-graph-pie"></i> Top 10 Selling Products of Current Year</h3>
               </div>
               <div class="panel-body">
                  <table class="table table-condensed">
                     <thead>
                        <tr>
                           <th>#</th>
                           <th>Product</th>
                           <th>Sold</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach($topSellingProducts['current_year'] as $key => $product)
                        @if($product->sold_qty)
                        <tr>
                           <td>{{ $key+1 }}</td>
                           <td><a href="{{ url('products') }}?name_code={{ $product->product_code }}" target="_blank">{{ $product->name }}</a></td>
                           <td>{{ $product->sold_qty }} {{ $product->unit }}</td>
                        </tr>
                        @endif
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
@stop

@section('script')
<script type="text/javascript">
jQuery(function(){

});

function viewPrint()
{
   var formData = $('form.stock-report').serialize();
   
   var url = '/reports/stock?' + formData;
        url += '&action=view';

   window.open(url);
}

function exportCsv()
{
   var formData = $('form.stock-report').serialize();
   
   var url = '/reports/stock?' + formData;
        url += '&action=export';

   window.open(url);
}
</script>
@stop
