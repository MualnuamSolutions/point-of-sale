@extends('print')

@section('content')
<h1 style="text-align:center">SALES REPORT
@if( $input['from'] == $input['to'])
{{ $input['from'] }}
@else
BETWEEN {{ $input['from'] }} to {{ $input['to'] }}
@endif
</h1>

<table border="0" width="100%" class="header">
   <tr>
      <td>Group: </td>
      <td align="right"><h3>ZOHANDCO</h3></td>
   </tr>
</table>

<table class="data">
   <thead>
      <tr>
         <th rowspan="2" align="middle">Sl.No</th>
         <th rowspan="2" align="middle">Particulars</th>
         <th colspan="3">Opening Stock</th>
      </tr>
      <tr>
         <th>Qty</th>
         <th>Rate</th>
         <th>Value</th>
      </tr>
   </thead>
   <tbody>
      <tr>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
   </tbody>
</table>
@stop

