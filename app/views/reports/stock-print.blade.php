@extends('print')

@section('content')
<h1 style="text-align:center">STOCK REPORT
@if( $input['from'] == $input['to'])
{{ $input['from'] }}
@else
BETWEEN {{ date('d/m/Y', strtotime($input['from'])) }} to {{ date('d/m/Y', strtotime($input['to'])) }}
@endif
</h1>

<table border="0" class="header">
   <tr>
      <td width="33%">{{ ($group != null) ? 'Group: ' . $group->name : '' }}</td>
      <td width="33%" align="right"><h3>ZOHANDCO</h3></td>
   </tr>
</table>

<div class="data-head">
   <div class="row">
      <div class="col" style="width:50px">Sl No</div>
      <div class="col" style="width:242px">Particulars</div>
      <div class="col has-sub" style="width:318px">
         <div class="sub-row">
            <div class="sub-col" style="width:317px">Opening Stock</div>
         </div>
         <div class="sub-row">
            <div class="sub-col" style="width:105px">Qty</div>
            <div class="sub-col" style="width:106px">Rate</div>
            <div class="sub-col" style="width:106px">Value</div>
         </div>
      </div>
      <div class="col has-sub" style="width:318px">
         <div class="sub-row">
            <div class="sub-col" style="width:317px">Stock Receives</div>
         </div>
         <div class="sub-row">
            <div class="sub-col" style="width:105px">Qty</div>
            <div class="sub-col" style="width:106px">Rate</div>
            <div class="sub-col" style="width:106px">Value</div>
         </div>
      </div>
      <div class="col has-sub" style="width:318px">
         <div class="sub-row">
            <div class="sub-col" style="width:317px">Sales</div>
         </div>
         <div class="sub-row">
            <div class="sub-col" style="width:105px">Qty</div>
            <div class="sub-col" style="width:106px">Rate</div>
            <div class="sub-col" style="width:106px">Value</div>
         </div>
      </div>
      <div class="col has-sub" style="width:212px">
         <div class="sub-row">
            <div class="sub-col" style="width:211px">Distribution</div>
         </div>
         <div class="sub-row">
            <div class="sub-col" style="width:105px">Qty</div>
            <div class="sub-col" style="width:106px">Value</div>
         </div>
      </div>
      <div class="col" style="width:130px">Stock Balance</div>
   </div>
</div>
<div class="data-body">
   @foreach($products as $key => $product)
   <div class="row">
      <div class="col" style="width:50px">{{ ++$key }}</div>
      <div class="col" style="width:242px">{{ $product->name }}<br><small>{{ $product->product_code }}</small></div>
      
      <?php $opening_stock = $product->opening_stock_qty - $product->opening_sold_qty; ?>
      <div class="col" style="width:105px" align="right">{{ $opening_stock.' '.$product->unit}}</div>
      <div class="col" style="width:106px" align="right">{{ $opening_stock ? '&#8377;'.$product->cp : null }}</div>
      <div class="col" style="width:107px" align="right">{{ $opening_stock ? '&#8377;'.$opening_stock * $product->cp : null }}</div>
      
      <div class="col" style="width:105px" align="right">{{ $product->received_qty ? round($product->received_qty).' '.$product->unit : null }}</div>
      <div class="col" style="width:106px" align="right">{{ $product->received_qty ? '&#8377;'.$product->cp:null }}</div>
      <div class="col" style="width:107px" align="right">{{ $product->received_qty ? '&#8377;'.$product->received_qty * $product->cp : null }}</div>
      
      <div class="col" style="width:105px" align="right">{{ $product->sold_qty ? round($product->sold_qty).' '.$product->unit : null }}</div>
      <div class="col" style="width:106px" align="right">{{ $product->sold_qty ? '&#8377; '.$product->sp : '' }}</div>
      <div class="col" style="width:107px" align="right">{{ $product->sold_qty ? '&#8377;'.$product->sold_total : '' }}</div>

      <div class="col" style="width:105px" align="right">{{ $product->distribution_qty ? round($product->distribution_qty).' '.$product->unit : null }}</div>
      <div class="col" style="width:107px" align="right">{{ $product->distribution_qty ? '&#8377;'.$product->distribution_qty * $product->cp:null }}</div>

      <div class="col" style="width:130px" align="right">{{ ($opening_stock + $product->received_qty) - $product->sold_qty - $product->distribution_qty }} {{ $product->unit }}</div>
   </div>
   @endforeach
</div>
@stop

