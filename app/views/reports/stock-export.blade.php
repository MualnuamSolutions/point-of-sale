<html>
<table>
   <thead>
      <tr>
         <th colspan="8">{{ $excelTitle }}</th>
         <th colspan="7">{{ ($group != null) ? 'Group: ' . $group->name : '' }}</th>
      </tr>
      <tr><th colspan="15"></th></tr>
      <tr>
         <th rowspan="2">SL NO</th>
         <th rowspan="2">PARTICULARS</th>
         <th rowspan="2">PRODUCT CODE</th>
         <th colspan="3">OPENING STOCK</th>
         <th colspan="3">STOCK RECEIVES</th>
         <th colspan="3">SALES</th>
         <th colspan="2">DISTRIBUTION</th>
         <th rowspan="2">STOCK BALANCE</th>
      </tr>
      <tr>
         <th></th>
         <th></th>
         <th></th>
         <th>Quantity</th>
         <th>Rate</th>
         <th>Value</th>
         <th>Quantity</th>
         <th>Rate</th>
         <th>Value</th>
         <th>Quantity</th>
         <th>Rate</th>
         <th>Value</th>
         <th>Quantity</th>
         <th>Value</th>
      </tr>
   </thead>
   <tbody>
      @foreach($products as $key => $product)
      <tr>
         <td>{{ ++$key }}</td>
         <td>{{ $product->name }}</td>
         <td>{{ $product->product_code }}</td>
         <?php $opening_stock = $product->opening_stock_qty - $product->opening_sold_qty; ?>
         <td>{{ $opening_stock.' '.$product->unit}}</td>
         <td>{{ $opening_stock ? '&#8377;'.$product->cp : null }}</td>
         <td>{{ $opening_stock ? '&#8377;'.$opening_stock * $product->cp : null }}</td>
         
         <td>{{ $product->received_qty ? round($product->received_qty).' '.$product->unit : null }}</td>
         <td>{{ $product->received_qty ? '&#8377;'.$product->cp:null }}</td>
         <td>{{ $product->received_qty ? '&#8377;'.$product->received_qty * $product->cp : null }}</td>
         
         <td>{{ $product->sold_qty ? round($product->sold_qty).' '.$product->unit : null }}</td>
         <td>{{ $product->sold_qty ? '&#8377; '.$product->sp : '' }}</td>
         <td>{{ $product->sold_qty ? '&#8377;'.$product->sold_total : '' }}</td>

         <td>{{ $product->distribution_qty ? round($product->distribution_qty).' '.$product->unit : null }}</td>
         <td>{{ $product->distribution_qty ? '&#8377;'.$product->distribution_qty * $product->cp:null }}</td>
         
         <td>{{ ($opening_stock + $product->received_qty) - $product->sold_qty - $product->distribution_qty }} {{ $product->unit }}</td>
      </tr>
      @endforeach
   </tbody>
</table>
</html>
