@extends('layout')

@section('content')
    <h4>Welcome, {{ $logged_user->name }}</h4>

    @if($logged_user->inGroup($Manager) || $logged_user->inGroup($StoreManager) || $logged_user->isSuperUser())
    <p class="help-block">Here's an overview of your store</p>
    @elseif($logged_user->inGroup($SalesPerson))
    <p class="help-block">Here's an overview of the outlet</p>
    @endif

    <div class="row dashboard">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <div class="dashboard-item item1">
                        <p>
                            <a href="{{ route('products.index') }}">
                            @if($logged_user->inGroup($Manager) || $logged_user->inGroup($StoreManager) || $logged_user->isSuperUser())
                            <span>{{ Products::count() }}</span><i class="fa fa-gift"></i>
                            <small>Product{{ Products::count() > 1 ? 's':'' }}</small>
                            @elseif($logged_user->inGroup($SalesPerson))
                            <?php $products = OutletsStocks::whereOutletId($logged_user->outlet_id)->count(); ?>
                            <span>{{ $products }}</span><i class="fa fa-gift"></i>
                            <small>Product{{ $products > 1 ? 's' : '' }}</small>
                            @endif
                            </a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="dashboard-item item2">
                        <p>
                            <a href="{{ route('products.index', ['stock'=>'low_stock']) }}">
                            @if($logged_user->inGroup($Manager) || $logged_user->inGroup($StoreManager) || $logged_user->isSuperUser())
                            <span>{{ Products::lowStock()->count() }}</span><i class="fa fa-arrow-circle-o-down"></i>
                            <small>Low Stock</small>
                            @elseif($logged_user->inGroup($SalesPerson))
                            <span>{{ Products::lowStock($logged_user->outlet_id)->count() }}</span><i class="fa fa-arrow-circle-o-down"></i>
                            <small>Low Stock</small>
                            @endif
                            </a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="dashboard-item item3">
                        <p>
                            <a href="{{ route('products.index', ['stock'=>'out_of_stock']) }}">
                            @if($logged_user->inGroup($Manager) || $logged_user->inGroup($StoreManager) || $logged_user->isSuperUser())
                            <span>{{ Products::whereInStock(0)->count() }}</span><i class="fa fa-warning"></i>
                            <small>Out of Stock</small>
                            @elseif($logged_user->inGroup($SalesPerson))
                            <span>{{ OutletsStocks::whereOutletId($logged_user->outlet_id)->whereQuantity(0)->count() }}</span><i class="fa fa-warning"></i>
                            <small>Out of Stock</small>
                            @endif
                            </a>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="dashboard-item item4">
                        <p>
                            <a href="{{ route('stockreturns.index', ['status'=>'Pending...']) }}">
                            @if($logged_user->inGroup($Manager) || $logged_user->inGroup($StoreManager) || $logged_user->isSuperUser())
                            <span>{{ OutletsStocksReturns::whereStatus('Pending...')->count() }}</span><i class="fa fa-clock-o"></i>
                            <small>Pending Return</small>
                            @elseif($logged_user->inGroup($SalesPerson))
                            <span>{{ OutletsStocksReturns::whereOutletId($logged_user->outlet_id)->whereStatus('Pending...')->count() }}</span><i class="fa fa-clock-o"></i>
                            <small>Pending Return</small>
                            @endif
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 dashboard-row">
            @if($logged_user->inGroup($Manager) || $logged_user->inGroup($StoreManager)  || $logged_user->isSuperUser())
            <div class="row">
                <div class="col-md-7">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">Outlets Overview</h3>
                        </div>
                        <div class="panel-body">
                            <table class="outlets table table-striped">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Products</th>
                                        <th>Out of Stock</th>
                                        <th>Low Stock</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach(SalesOutlets::orderBy('name', 'asc')->get() as $outlet)
                                    <tr>
                                        <td>{{ $outlet->name }}</td>
                                        <td>{{ OutletsStocks::whereOutletId($outlet->id)->count() }}</td>
                                        <td>{{ OutletsStocks::whereOutletId($outlet->id)->whereQuantity(0)->count() }}</td>
                                        <td>{{ Products::lowStock($outlet->id)->count() }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                @if($logged_user->inGroup($Manager) || $logged_user->isSuperUser())
                <div class="col-md-5">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Fast Moving Products (For {{ date('Y') }})</h3>
                        </div>
                        <div class="panel-body">
                            <table class="outlets table table-striped">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Product</th>
                                        <th>Stock Intake</th>
                                        <th>Sold</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($fastMovingProducts as $key => $product)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $product->name }} ({{ $product->product_code }})</td>
                                    <td>{{ $product->current_stock_qty }} {{ $product->unit }}</td>
                                    <td>{{ $product->sold_qty }} {{ $product->unit }}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @endif

            </div>
            @endif
        </div>
    </div>
@stop
