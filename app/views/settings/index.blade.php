@extends('layout')

@section('content')
   <div class="col-md-6 col-md-offset-3">
      <div class="row">
         <div class="panel panel-default">
            <div class="panel-heading">
               <h3 class="panel-title"><i class="fa fa-cogs"></i> Settings</h3>
            </div>
            <div class="panel-body">
               {{ Form::open(['url' => route('settings.store'), 'method' => 'post', 'class' => 'form-vertical', 'autocomplete' => 'off']) }}
                  <div class="form-group {{ $errors->has('store_name') ? 'has-error' : '' }}">
                     {{ Form::label('store_name', 'Store Name') }}
                     {{ Form::text('store_name', Input::get('store_name', Settings::getData('store_name')), ['class' => 'form-control']) }}

                     @if($errors->has('store_name'))
                     <p class="help-block">{{ $errors->first('store_name') }}</p>
                     @endif
                  </div>

                  <div class="form-group {{ $errors->has('low_stock_warning_qty') ? 'has-error' : '' }}">
                     {{ Form::label('low_stock_warning_qty', 'Low Stock Warning Quantity') }}
                     {{ Form::text('low_stock_warning_qty', Input::get('low_stock_warning_qty', Settings::getData('low_stock_warning_qty')), ['class' => 'form-control']) }}

                     @if($errors->has('low_stock_warning_qty'))
                     <p class="help-block">{{ $errors->first('low_stock_warning_qty') }}</p>
                     @endif
                  </div>

                  <div class="form-group {{ $errors->has('fast_moving') ? 'has-error' : '' }}">
                     {{ Form::label('fast_moving', 'Display Limit For Fast Moving Products') }}
                     {{ Form::text('fast_moving', Input::get('fast_moving', Settings::getData('fast_moving')), ['class' => 'form-control']) }}

                     @if($errors->has('fast_moving'))
                     <p class="help-block">{{ $errors->first('fast_moving') }}</p>
                     @endif
                  </div>

                  <div class="for-group text-right">
                     {{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}
                  </div>
               {{ Form::close() }}
            </div>
         </div>
      </div>
   </div>
@stop
