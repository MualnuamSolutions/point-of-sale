<div class="filter">
   {{ Form::open(['url' => route('stockreturns.index'), 'method' => 'get', 'class' => 'form-inline', 'autocomplete' => 'off']) }}

      @if ($logged_user->isSuperUser() || ($logged_user && $logged_user->inGroup($Manager)) )
      <div class="form-group">
         {{ Form::select('outlet', $outlets, Input::get('outlet', null), ['class' => 'form-control input-sm']) }}
      </div>
      @endif
      
      <div class="form-group">
         {{ Form::select('status', ['' => 'All', 'Pending...' => 'Pending...', 'Approved' => 'Approved', 'Rejected' => 'Rejected'], Input::get('status', null), ['class' => 'form-control input-sm']) }}
      </div> 

   {{ Form::close() }}
   <hr>
</div>
