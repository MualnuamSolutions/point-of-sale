@extends('layout')

@section('content')
   <div class="col-md-12">
      <div class="row">
         <div class="panel panel-default">

            <div class="panel-heading">
               <h3 class="panel-title"><i class="fi-page-multiple"></i> Stocks Return</h3>
            </div>

            <div class="panel-body">
               @include('stockreturns._filter')

               <table class="table table-condensed">
                  <thead>
                     <tr>
                        <th>#</th>
                        @if( $logged_user->hasAccess(['stockreturns.approve', 'stockreturns.reject']) )
                        <th class="col-md-2">Outlet</th>
                        <th class="col-md-2">Product</th>
                        <th class="col-md-1">Quantity</th>
                        <th class="col-md-2">Comment</th>
                        <th class="col-md-2">Returned Date</th>
                        <th class="col-md-1">Status</th>
                        <th class="col-md-2"></th>
                        @else
                        <th class="col-md-2">Product</th>
                        <th class="col-md-1">Quantity</th>
                        <th class="col-md-4">Comment</th>
                        <th class="col-md-2">Returned Date</th>
                        <th class="col-md-1">Status</th>
                        <th class="col-md-2"></th>
                        @endif
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($returnsStocks as $returnstock)
                     <tr>
                        <td>{{ $returnstock->id }}</td>
                        @if( $logged_user->hasAccess(['stockreturns.approve', 'stockreturns.reject']) )
                        <td>{{ stripslashes($returnstock->outlet->name) }}</td>
                        @endif
                        <td>
                           <a href="{{ route('stockreturns.show', $returnstock->product->id) }}">{{ stripslashes($returnstock->product->name) }}</a><br />
                           <small>{{ $returnstock->product->product_code}}</small>
                        </td>
                        <td>{{ $returnstock->quantity }}</td>
                        <td>{{ $returnstock->comment }}</td>
                        <td>{{ date('d/m/Y', strtotime($returnstock->created_at)) }}</td>
                        <td>{{ $returnstock->status }}</td>
                        <td>
                           @if( $logged_user->hasAccess('stockreturns.approve') && $returnstock->status == "Pending..." )
                           <a href="{{ route('stockreturns.approve', $returnstock->id) }}" class="btn btn-sm btn-primary" onclick='return confirm("Are you sure you want to approve stock return? This action cannot be undone.")'><i class="fi-check"></i> Approve</a>
                           @endif
                           @if( $logged_user->hasAccess('stockreturns.reject') && $returnstock->status == "Pending..." )
                           <a href="{{ route('stockreturns.reject', $returnstock->id) }}" class="btn btn-sm btn-danger" onclick='return confirm("Are you sure you want to reject stock return? This action cannot be undone.")'><i class="fa fa-times"></i> Reject</a>
                           @endif
                        </td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
               
               {{ $returnsStocks->links() }}
            </div>
         </div>
      </div>
   </div>
@stop
